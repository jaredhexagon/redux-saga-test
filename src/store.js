import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import appReducer from './ducks/app/reducer'
import { watchLogin } from './ducks/app/sagas'

export default () => {
  const rootReducer = combineReducers({ app: appReducer })
  const sagaMiddleware = createSagaMiddleware()
  const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
  sagaMiddleware.run(watchLogin)
  return store
}
