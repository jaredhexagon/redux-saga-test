import createStore from './store'
import { login } from './ducks/app/actions'

const store = createStore()

store.subscribe(() => console.log(store.getState()))

store.dispatch(login('jared.williams@hexagon.com'))