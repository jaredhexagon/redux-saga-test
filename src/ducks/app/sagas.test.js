import { put } from 'redux-saga/effects'
import { loginSaga } from './sagas'
import { performLogin } from './fetch'
import { loggedIn } from './actions'

jest.mock('./fetch')
jest.mock('redux-saga/effects')

describe('App sagas', () => {
  describe('login', () => {
    describe('When called with a username', () => {
      beforeAll(() => {
        const gen = loginSaga('jared.williams@hexagon.com')
        gen.next()
        gen.next()
      })

      it('Posts to the endpoint', () => {
        expect(performLogin).toHaveBeenCalledWith('jared.williams@hexagon.com')
      })

      it('Dispatches isLoggedIn action', () => {
        expect(put).toHaveBeenCalledWith(loggedIn('jared.williams@hexagon.com'))
      })
    })
  })
})