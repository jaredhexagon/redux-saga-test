export const LOGIN = 'LOGIN'
export const login = username => ({
  type: LOGIN,
  payload: {
    username
  }
})

export const LOGGED_IN = 'LOGGED_IN'
export const loggedIn = username => ({
  type: LOGGED_IN,
  payload: {
    username
  }
})