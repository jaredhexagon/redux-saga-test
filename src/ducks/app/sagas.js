import { put, takeLatest } from 'redux-saga/effects'
import { loggedIn, LOGIN } from './actions'
import { performLogin } from './fetch'

export function* loginSaga(username) {
  yield performLogin(username)
  yield put(loggedIn(username))
}

export function* watchLogin() {
  yield takeLatest(LOGIN, action => loginSaga(action.payload.username))
}