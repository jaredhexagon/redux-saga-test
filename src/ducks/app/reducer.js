import { LOGGED_IN } from './actions'

const defaultState = {
  isLoggedIn: false,
  username: null
}

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case LOGGED_IN:
      return {
        ...state,
        isLoggedIn: true,
        username: action.payload.username
      }
    default:
      return state
  }
}
